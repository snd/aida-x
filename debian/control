Source: aida-x
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Dennis Braun <snd@debian.org>,
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 dpf-source:native,
 libasound2-dev,
 libdbus-1-dev,
 libgl-dev,
 librtaudio-dev,
 librtmidi-dev,
 libx11-dev,
 libxext-dev,
 libxrandr-dev,
 pkgconf,
 python3,
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/multimedia-team/aida-x
Vcs-Git: https://salsa.debian.org/multimedia-team/aida-x.git
Homepage: https://github.com/AidaDSP/AIDA-X
Rules-Requires-Root: no

Package: aida-x
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Provides:
 clap-plugin,
 lv2-plugin,
 standalone-plugin,
 vst-plugin,
 vst3-plugin,
Description: Amp Model Player leveraging AI
 AIDA-X is an Amp Model Player, allowing it to load models of AI trained music
 gear, which you can then play through!
 .
 Its main intended use is to provide high fidelity simulations of amplifiers.
 However, it is also possible to run entire signal chains consisting of any
 combination of amp, cab, dist, drive, fuzz, boost and eq.
 .
 For ease of use, this plugin also contains a cabinet simulator via impulse
 response files, which runs after the Amp Model.
 .
 CLAP, LV2, VST2 and VST3 plugin formats are supported, plus a standalone.
